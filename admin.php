<?php
include "conn.php";

if(!isset($_SESSION['username'])){
    header("Location: home.php");
}

$q = mysqli_query($conn,"SELECT COUNT(dk_personil.nama) 'jumlahPersonil', dk_pos_damkar.namaPos,dk_pos_damkar.lat,dk_pos_damkar.long,dk_kelurahan.namaKelurahan FROM dk_personil RIGHT JOIN dk_pos_damkar on dk_personil.idPos = dk_pos_damkar.idPos INNER JOIN dk_kelurahan ON dk_kelurahan.idKelurahan = dk_pos_damkar.idKelurahan GROUP BY dk_pos_damkar.namaPos");
$emp = [];
while($data = mysqli_fetch_assoc($q)){
    $emp[] = $data;
}
$query_hidran = mysqli_query($conn, "SELECT * FROM dk_hidran inner join dk_komponen_hidran on dk_hidran.idKomponen = dk_komponen_hidran.idKomponen");
$emp1 = [];

while($data1 = mysqli_fetch_assoc($query_hidran)){
    $emp1[] = $data1;
}


?> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
        <link rel="stylesheet" href="styles.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,500;0,900;1,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript">

            var map = null, infoboxlayer, dataLayer;
            var loadData = <?php echo json_encode($emp, true); ?>;
            var dataHidran = <?php echo json_encode($emp1, true); ?>;  

            function GetMap() {
                // Initialize the map
                map = new Microsoft.Maps.Map(document.getElementById("myMap"),
                            { credentials: "AkaA4bp_CbMQZvMmURVKQpZIYB40hs2DNymYiISLYnxEbel76CrYqup1AQ8ILX9_", 
                            center:new Microsoft.Maps.Location(-7.8011131,110.3897088),
                            zoom:12 });

                dataLayer = new Microsoft.Maps.EntityCollection();
                map.entities.push(dataLayer);

                var infoboxLayer = new Microsoft.Maps.EntityCollection();
                map.entities.push(infoboxLayer);

                infobox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(-7.8011131,110.3897088), { visible: false, offset: new Microsoft.Maps.Point(0, 20) });
                infoboxLayer.push(infobox);

                AddData();
            }
        
            function AddData() {
                var i;
                for(i = 0; i < loadData.length ; i++){
                    var pin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(loadData[i]['lat'], loadData[i]['long']),{icon:'img/damkar.png'});
                    pin.color = "red";
                    pin.Title = loadData[i]['namaPos'];
                    pin.Description = "Kelurahan : " + loadData[i]['namaKelurahan'] + "<br>" + "Jumlah Personil : " + loadData[i]['jumlahPersonil'] + " personil";
                    Microsoft.Maps.Events.addHandler(pin, 'click', displayInfobox);
                    dataLayer.push(pin);
                }
                var x;
                for(x = 0; x < dataHidran.length ; x++){
                    var pin1;
                    if(dataHidran[x]['idKomponen']==1){
                        pin1 = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(dataHidran[x]['latitude'], dataHidran[x]['longitude']),{icon:'img/hidrant.png'});
                    }
                    if(dataHidran[x]['idKomponen']==2){
                        pin1= new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(dataHidran[x]['latitude'], dataHidran[x]['longitude']),{icon:'img/boxhidrant.png'});
                    }
                    if(dataHidran[x]['idKomponen']==3){
                        pin1 = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(dataHidran[x]['latitude'], dataHidran[x]['longitude']),{icon:'img/siamese.png'});
                    }
                   
                    pin1.color = "red";
                    pin1.Title = "Hidran";
                    pin1.Description = "Kategori : " + dataHidran[x]['kategori'] + "<br>" + "Komponen : "+ dataHidran[x]['jenisHidran'];
                    Microsoft.Maps.Events.addHandler(pin1, 'click', displayInfobox);
                    dataLayer.push(pin1);
                }
            }
            function displayInfobox(e) {
                if (e.targetType == 'pushpin') {
                    infobox.setLocation(e.target.getLocation());
                    infobox.setOptions({ visible: true, title: e.target.Title, description: e.target.Description, icon: e.target.Icon});
                }
            }  
            function filteringSelection(){
          }
        </script>
    </head>

    <nav>
        
        <ul class="horizontal">
            <li><img src="img/logo.png" width="47" height="57"  alt=""></li>
            <li><p>DINAS PEMADAM KEBAKARAN DAN <br>PENYELAMATAN KOTA YOGYAKARTA</p></li>
            <li><a class="active" href="javascript:void(0)">Home</a></li>
            <li><a href="form.php">Form</a></li>
            <li><a href="detil.php">Details</a></li>
            <li class="rightli" style="float:right"><a href="destroy.php">Logout</a></li>
        </ul>
    </nav>
    
    <body onload="GetMap();">
    
        <div id="myMap"></div>
   
            
            <div class="container-card">
                <div class="card">
                    <img src="img/kawasan.png" alt="icon kawasan" style="width:100%">
                    <h1>Kawasan</h1>
                    
                    <p>Pengisian form <br>kawasan</p>
                    <p><button onclick="window.location.href='list/list-kawasan.php'">Detail</button></p>
                </div>
                <div class="card">
                    <img src="img/gedung.png" alt="icon gedung" style="width:100%">
                    <h1>Gedung</h1>
                    
                    <p>Pengisian form <br>gedung</p>
                    <p><button onclick="window.location.href='list/list-gedung.php'">Detail</button></p>
                </div>
                <div class="card">
                    <img src="img/pos.png" alt="icon pos" style="width:100%">
                    <h1>Pos DAMKAR</h1>
                    
                    <p>Pengisian form <br>pos pemadam kebakaran</p>
                    <p><button onclick="window.location.href='list/list-pos.php'">Detail</button></p>
                </div>
        </div>
    </body>
    
<footer class="footer" style="background-image: linear-gradient(rgba(10, 75, 120,.85), rgba(10, 75, 120,.85)), url('https://kebakaran.jogjakota.go.id/assets/public/batik.png')">
        	<div class="container ">
        		<div class="row">
                    <div class="footer-text">
                        <span>DINAS PEMADAM KEBAKARAN DAN PENYELAMATAN KOTA YOGYAKARTA © 2022 <a href="http://jogjakota.go.id" class="text-light"> Pemerintah Kota Yogyakarta</a></span><br>
                        <span>
                            Jl. Kenari No. 56 Yogyakarta                             Telp.&nbsp;(0274) 587101                             Fax.&nbsp;(0274) 587101                             Email&nbsp;:&nbsp;kebakaran@jogjakota.go.id                        </span>
                    </div>       			
        		</div>
        	</div>
        </footer>
</html>
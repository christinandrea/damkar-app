<?php
require "../conn.php";
$cmd = "SELECT * FROM dk_kawasan LEFT JOIN dk_tipe_kawasan 
            ON dk_kawasan.idTipeKawasan = dk_tipe_kawasan.idTipeKawasan 
            LEFT JOIN dk_detail_kawasan ON dk_detail_kawasan.idKawasan = dk_kawasan.idKawasan
            LEFT JOIN dk_akses_kendaraan ON dk_kawasan.idAksesKendaraan = dk_akses_kendaraan.idAksesKendaraan ";

$cmd2 = "SELECT * FROM dk_kawasan NATURAL JOIN dk_tipe_kawasan NATURAL JOIN dk_detail_kawasan NATURAL JOIN dk_akses_kendaraan ORDER BY dk_kawasan.idKawasan ASC";

$query = mysqli_query($conn, $cmd2);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
    <link rel="stylesheet" href="../style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,500;0,900;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <title>Document</title>
</head>

<body>
    <nav>
        <ul class="horizontal">
            <li><img src="../img/logo.png" width="47" height="57" alt=""></li>
            <li>
                <p>DINAS PEMADAM KEBAKARAN DAN <br>PENYELAMATAN KOTA YOGYAKARTA</p>
            </li>
            <li><a href="../admin.php">Home</a></li>
            <li><a class="active" href="../form.php">Form</a></li>
            <li><a href="../detil.php">Details</a></li>
            <li class="rightli" style="float:right"><a href="javascript:void(0)">Logout</a></li>
        </ul>
    </nav>
    <div class="link-container">
        <a class="btn btn-primary" href="../form/form-kawasan.php">Tambah Kawasan</a>
    </div>
    <div class="table-container">
        <br>
        <div class="table-container">
            <table class="table">

                <thead>
                    <tr>
                        <th scope="row">ID Kawasan</th>
                        <th scope="row">Nama Kawasan </th>
                        <th scope="row">Luas</th>
                        <th scope="row">Resiko</th>
                        <th scope="row">Jenis Kawasan</th>
                        <th scope="row">Jumlah Hidran</th>
                        <th scope="row">Lebar Akses Kendaraan</th>
                    </tr>



                    <!-- <th>Nama Kawasan </th>
        <th>Nama Kawasan </th>
        <th>Nama Kawasan </th> -->

                </thead>
                <tbody>
                    <?php

                    if (mysqli_num_rows($query) > 0) {
                        while ($row = mysqli_fetch_assoc($query)) {
                            echo ("
                <tr>
                <td>" . $row['idKawasan'] . "</td>
                <td>" . $row['namaKawasan'] . "</td>
                <td>" . $row['luasKawasan'] . "</td>
                <td>" . $row['levelKlasifikasiResiko'] . "</td>
                <td>" . $row['jenisKawasan'] . "</td>
                <td>" . $row['jumlahHidran'] . "</td>
                <td>" . $row['lebarArea'] . " Meter</td>
                
                </tr>
                
                ");
                        }
                    }

                    ?>
                </tbody>
            </table>
        </div>
    </div>  
    <footer class="footer" style="background-image: linear-gradient(rgba(10, 75, 120,.85), rgba(10, 75, 120,.85)), url('https://kebakaran.jogjakota.go.id/assets/public/batik.png')">
        	<div class="container ">
        		<div>
                    <div class="footer-text">
                        <span>DINAS PEMADAM KEBAKARAN DAN PENYELAMATAN KOTA YOGYAKARTA © 2022 <a href="http://jogjakota.go.id" class="text-light"> Pemerintah Kota Yogyakarta</a></span><br>
                        <span>
                            Jl. Kenari No. 56 Yogyakarta                             Telp.&nbsp;(0274) 587101                             Fax.&nbsp;(0274) 587101                             Email&nbsp;:&nbsp;kebakaran@jogjakota.go.id                        </span>
                    </div>       			
        		</div>
        	</div>
    </footer>
</body>
<style>
    .link-container {
        padding: 20px 20px 20px 20px;
    }

    .table-container {
        padding: 10px 50px 10px 50px;
    }
</style>

</html>
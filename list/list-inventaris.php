<?php
include "../conn.php";

$get = "SELECT * FROM dk_inventaris_pos inner join dk_pos_damkar on dk_inventaris_pos.idPos = dk_pos_damkar.idPos";
$query = mysqli_query($conn, $get);



?>


<!DOCTYPE html>
<head>

</head>

<body>
<div class="container">
    <div class="link-container">
        <a href="../form/form-inventaris.php">+ Tambah Data Inventaris </a>
        <a href="../form/form-pos.php">+ Tambah Pos DAMKAR </a>
        <a href="../form/form-alat-pemadam.php">+ Tambah Alat Pemadam </a>
        <a href="../form/form-detail-transport.php">+ Tambah Transportasi </a>
    </div>
    <div class="table-container">

    <table>
        <thead>
            <th>No. Inventaris</th>
            <th>Nama Pos</th>
            
        </thead>
        <tbody>
            <?php

            if(mysqli_num_rows($query)>0){
                while($row=mysqli_fetch_array($query)){
                    echo ("
                    <tr>
                    <td>".$row['idInventaris']."</td>
                    <td>".$row['namaPos']."</td>
                    <td><a href='../form/form-detail-inventaris.php?idinventaris=".$row['idInventaris']."' class='button-control'>+Tambah Data</a></td>
                    <td><a href='list-inventaris-by-id.php?idinventaris=".$row['idInventaris']."' class='button-control'>Lihat Data</a></td>
                    
                    ");
                }
            }

            ?>
        </tbody>
    </table>
    </div>
</div>
</body>
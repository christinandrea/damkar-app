
<?php
include "../conn.php";

if(isset($_POST['submit'])){
    $id  = $_POST['idpos'];
    $nama = $_POST['name-pos'];
    $lat = $_POST['lat-pos'];
    $long = $_POST['long-pos'];
    $kelurahan = $_POST['kelurahan'];

    $insert = "INSERT INTO dk_pos_damkar VALUES('$id','$nama','$lat','$long','$kelurahan')";
    $query = mysqli_query($conn, $insert);

    if($query){
        echo "<script>alert('Data berhasil ditambahkan!')</script>";
        echo "<script>window.location.href='../list/list-pos.php'</script>";
    }else{
        echo "<script>alert('Data Gagal ditambahkan!')</script>";
    }
}


?>

<!DOCTYPE html>
<html>
    <head>
    <title>
        Form Pos DAMKAR
    </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
        <link rel="stylesheet" href="styles.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,500;0,900;1,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <nav>
        <ul class="horizontal">
            <li><img src="../img/logo.png" width="47" height="57"  alt=""></li>
            <li><p>DINAS PEMADAM KEBAKARAN DAN <br>PENYELAMATAN KOTA YOGYAKARTA</p></li>
            <li><a href="../admin.php">Home</a></li>
            <li><a class="active" href="../admin.php">Form</a></li>
            <li><a href="../detil.php">Details</a></li>
            <li class="rightli" style="float:right"><a href="javascript:void(0)">Logout</a></li>
        </ul>
    </nav>

    <body>
        <div class="col-md-7 col-lg-8">
            <form action="form-pos.php" method="post">
                <div class="row g-3">
                    <?php

                    $get = "SELECT max(idPos) as newID from dk_pos_damkar";
                    $query = mysqli_query($conn, $get);
                    $arr = mysqli_fetch_array($query);
                    $newID = $arr['newID'];

                    $code = (int) substr($newID, 3, 3);
                    $code++;
                    $char = "POS";
                    $newCode = $char.sprintf("%03s",$code);

                    ?>
                    <div class="form-label">
                        <input type="text" readonly="" name="idpos" value="<?php echo $newCode; ?>" hidden>
                    </div>

                    <div class="col-sm-12">
                        <label class="form-label">Nama Pos</label>
                        <input type="text" name="name-pos" class="form-control">
                    </div>
                    <div class="col-sm-12">
                        <?php
                            $data = mysqli_query($conn, "SELECT * FROM dk_kelurahan");
                        ?>

                        <label class="form-label">Kelurahan </label>
                        <select name="kelurahan" id="kelurahan" class="form-control">
                            <option value="">-- Pilih Kelurahan</option>

                        <?php
                            while($row = mysqli_fetch_array($data,MYSQLI_ASSOC)):; 
                        ?>

                            <option value="<?php echo $row['idKelurahan']; ?>"><?php echo $row['namaKelurahan'] ?></option>
                        
                        <?php
                            endwhile;
                        ?>
                        </select>
                    </div>
                   
                        
                    <div class="col-sm-6">
                        <label class="form-label">Latitude</label>
                        <input type="text" name="lat-pos" class="form-control">
                    </div>
            
                    <div class="col-sm-6">
                        <label class="form-label">Longitude</label>
                        <input type="text" name="long-pos" class="form-control">
                    </div>
                
                <div class="col-sm-12">

            <hr class="my-4">

            </div>
            <div class="col-sm-3">

            <button type="submit" name="submit" class="btn btn-primary btn-lg" onclick="window.location.href='../list/list-kawasan.php'" > Tambah Data </button>

            </div>
            
            </form>
            </div>

        </div>

</body>
<footer class="footer" style="background-image: linear-gradient(rgba(10, 75, 120,.85), rgba(10, 75, 120,.85)), url('https://kebakaran.jogjakota.go.id/assets/public/batik.png')">
        	<div class="container ">
        		<div>
                    <div class="footer-text">
                        <span>DINAS PEMADAM KEBAKARAN DAN PENYELAMATAN KOTA YOGYAKARTA © 2022 <a href="http://jogjakota.go.id" class="text-light"> Pemerintah Kota Yogyakarta</a></span><br>
                        <span>
                            Jl. Kenari No. 56 Yogyakarta                             Telp.&nbsp;(0274) 587101                             Fax.&nbsp;(0274) 587101                             Email&nbsp;:&nbsp;kebakaran@jogjakota.go.id                        </span>
                    </div>       			
        		</div>
        	</div>
    </footer>
</html>
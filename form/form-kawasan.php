<?php
require "../conn.php";

if(isset($_POST['submit'])){
   
    $id = $_POST['idKawasan'];
    $nama = $_POST['namaKawasan'];
    $luas = $_POST['luasKawasan'];
    $risiko = $_POST['level'];
    $jenis = $_POST['tipe'];
    $idkend = $_POST['akses'];
    $jumhidran = $_POST['jumlahHidran'];

    $insert = "INSERT INTO dk_kawasan(idKawasan,namaKawasan, luasKawasan, idTipeKawasan, levelKlasifikasiResiko, idAksesKendaraan,jumlahHidran) 
                VALUES('$id','$nama','$luas','$jenis','$risiko','$idkend','$jumhidran')";
    $query = mysqli_query($conn, $insert);
    $latA = $_POST['latA'];
    $longA = $_POST['longA'];

    $latB = $_POST['latB'];
    $longB = $_POST['longB'];

    $latC = $_POST['latC'];
    $longC = $_POST['longC'];

    $latD = $_POST['latD'];
    $longD = $_POST['longD'];

    $insert2 = "INSERT INTO dk_detail_kawasan(idKawasan, latitude, longitude, latitudeB, longitudeB, latitudeC, longitudeC, latitudeD, longitudeD) 
                VALUES('$id','$latA','$longA','$latB','$longB','$latC','$longC','$latD','$longD')";
    $query2 = mysqli_query($conn, $insert2);

    if($query && $query2){
        echo "<script>alert('Data berhasil ditambahkan!')</script>";
        echo "<script>window.location.href='../list/list-kawasan.php'</script>";
    }else{
        echo "Failed";
    }
}


?>

<!DOCTYPE html>
<head>
    <title>
        Form Tipe Kawasan
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
        <link rel="stylesheet" href="styles.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,500;0,900;1,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    
</head>


<body>
    <nav>
        <ul class="horizontal">
            <li><img src="../img/logo.png" width="47" height="57"  alt=""></li>
            <li><p>DINAS PEMADAM KEBAKARAN DAN <br>PENYELAMATAN KOTA YOGYAKARTA</p></li>
            <li><a href="../admin.php">Home</a></li>
            <li><a class="active" href="../admin.php">Form</a></li>
            <li><a href="../detil.php">Details</a></li>
            <li class="rightli" style="float:right"><a href="javascript:void(0)">Logout</a></li>
        </ul>
    </nav>
    <div class="col-md-7 col-lg-8">
        <form action="form-kawasan.php" method="post">    
            <div class="row g-3">
                
                <div class="col-sm-6">
                    <label class="form-label">ID Kawasan</label>
                    <input type="text" name="idKawasan" class="form-control">
                </div>
                <div class="col-sm-6">
                    <label class="name-input">Nama Kawasan</label>
                    <input type="text" name="namaKawasan" class="form-control"> 
                </div>

                
                <div class="col-sm-12">
                    <label class="name-input">Luas Kawasan</label>
                    <input type="text" name="luasKawasan" class="form-control"> 
                </div>



                <div class="col-sm-6">
                    <label class="name-input">Latitude A</label>
                    <input type="text" name="latA" class="form-control"> 
                </div>
                <div class="col-sm-6">
                    <label class="name-input">Longitude A</label>
                    <input type="text" name="longA" class="form-control"> 
                </div>

                <div class="col-sm-6">
                    <label class="name-input">Latitude B</label>
                    <input type="text" name="latB" class="form-control"> 
                </div>
                <div class="col-sm-6">
                    <label class="name-input">Longitude B</label>
                    <input type="text" name="longB" class="form-control"> 
                </div>

                <div class="col-sm-6">
                    <label class="name-input">Latitude C</label>
                    <input type="text" name="latC" class="form-control"> 
                </div>
                <div class="col-sm-6">
                    <label class="name-input">Longitude C</label>
                    <input type="text" name="longC" class="form-control"> 
                </div>

                <div class="col-sm-6">
                    <label class="name-input">Latitude D</label>
                    <input type="text" name="latD" class="form-control"> 
                </div>
                <div class="col-sm-6">
                    <label class="name-input">Longitude D</label>
                    <input type="text" name="longD" class="form-control"> 
                </div>
                <div class="col-sm-6">
                    <label class="name-input">Level Klasifikasi Risiko</label>
                    <select name="level" class="form-control">
                        <option value="">--Pilih Klasifikasi--</option>
                        <option value="Bahaya">Bahaya</option>
                        <option value="Rawan">Rawan</option>
                        <option value="Aman">Aman</option>
                    </select>
                </div>
                <div class="col-sm-6">
                    <label class="name-input">Jumlah Hidran</label>
                    <input type="text" name="jumlahHidran" class="form-control"> 
                </div>
            
                <div class="col-sm-6">
                <?php
                $data = mysqli_query($conn, "SELECT * FROM dk_tipe_kawasan ");
                
                ?>
                <label class="name-input">Tipe Kawasan </label>
                <select name="tipe" id="tipe" class="form-control">
                <option value="">-- Pilih Tipe Kawasan</option>
                <?php
            
                    while($row = mysqli_fetch_array($data,MYSQLI_ASSOC)):; 
                    ?>
                
                    <option value="<?php echo $row['idTipeKawasan']; ?>"><?php echo $row['jenisKawasan'] ?></option>
                    <?php
                    endwhile;
                    ?>
            </select>
            </div>
                <div class="col-sm-6">
 
                
            <label class="name-input">Akses Kendaraan </label>
            <select name="akses" id="akses" class="form-control">
            <option value="">-- Pilih Akses Kendaraan</option>
            <?php
                $data = mysqli_query($conn, "SELECT * FROM dk_akses_kendaraan");
                while($row = mysqli_fetch_array($data,MYSQLI_ASSOC)):; 
                ?>
               
                <option value="<?php echo $row['idAksesKendaraan']; ?>"><?php echo $row['lebarArea'] ?> Meter</option>
                <?php
                endwhile;
                ?>
            </select>
            </div>
            <div class="col-sm-12">

            <hr class="my-4">

            </div>
            <div class="col-sm-3">

            <button type="submit" name="submit" class="btn btn-primary btn-lg" onclick="window.location.href='../list/list-kawasan.php'" > Tambah Data </button>

            </div>

            
            </div>
            

            </form>
        
    </div>
</body>
<footer class="footer" style="background-image: linear-gradient(rgba(10, 75, 120,.85), rgba(10, 75, 120,.85)), url('https://kebakaran.jogjakota.go.id/assets/public/batik.png')">
        	<div class="container ">
        		<div>
                    <div class="footer-text">
                        <span>DINAS PEMADAM KEBAKARAN DAN PENYELAMATAN KOTA YOGYAKARTA © 2022 <a href="http://jogjakota.go.id" class="text-light"> Pemerintah Kota Yogyakarta</a></span><br>
                        <span>
                            Jl. Kenari No. 56 Yogyakarta                             Telp.&nbsp;(0274) 587101                             Fax.&nbsp;(0274) 587101                             Email&nbsp;:&nbsp;kebakaran@jogjakota.go.id                        </span>
                    </div>       			
        		</div>
        	</div>
    </footer>
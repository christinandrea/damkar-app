
<?php
include "../conn.php";
$id = $_GET['idinventaris'];


if(isset($_POST['submit'])){
    $newID = mysqli_real_escape_string($conn,$_POST['id-detail']);
    $alat = $_POST['alat'];
    $plat = $_POST['transport'];
    $jumlah = $_POST['jumlah-alat'];


  
    // $nama = $_POST['name-pos'];
    // $lat = $_POST['lat-pos'];
    // $long = $_POST['long-pos'];
    // $desa = $_POST['desa'];

    $insert = "INSERT INTO dk_detail_inventaris VALUES((SELECT idInventaris from dk_inventaris_pos where idInventaris='$newID'),'$alat','$plat','$jumlah')";
    $query = mysqli_query($conn, $insert);

    if($query){
        echo "query ok";
    }else{
        echo "query failed";
    }
}


?>

<!DOCTYPE html>
<head>
    <title>
        Form Detail Inventaris
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
        <link rel="stylesheet" href="styles.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,500;0,900;1,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    
        
</head>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="home.php">DAMKAR</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../home.php">Home </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../form.php">Form <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="../detil.php">Detail</a>
                </li>
            </ul>
        </div>
    </nav>

<body>
<div class="col-md-7 col-lg-8">
        
            <form action="form-detail-inventaris.php" method="post">
            <div class="row g-3">
            <input type="text" value="<?php echo $id; ?>" readonly="" name="id-detail">

            <div class="col-sm-6">
            <?php
            include "../conn.php";
            $data = mysqli_query($conn, "SELECT * FROM dk_alat_pemadam");
            
            ?>
            <label class="name-input">Alat Pemadam </label>
            <select name="alat" id="alat" class="form-control">
            <option value="">-- Pilih Alat Pemadam</option>
            <?php
          
                while($row = mysqli_fetch_array($data,MYSQLI_ASSOC)):; 
                ?>
               
                <option value="<?php echo $row['idAlatPemadam']; ?>"><?php echo $row['namaAlat'] ?></option>
                <?php
                endwhile;
                ?>
            </select>
            </div>

            <div class="col-sm-6">
            <?php
            include "../conn.php";
            $data = mysqli_query($conn, "SELECT * FROM dk_detail_transport INNER JOIN dk_tipe_transport on dk_detail_transport.idTipeTransport = dk_tipe_transport.idTipeTransport");
            
            ?>
            <label class="name-input">Plat Nomor dan Tipe Transportasi </label>
            <select name="transport" id="transport" class="form-control">
            <option value="">-- Pilih Plat Nomor dan Tipe Transportasi</option>
            <?php
          
                while($row = mysqli_fetch_array($data,MYSQLI_ASSOC)):; 
                ?>
               
                <option value="<?php echo $row['platNomor']; ?>"><?php echo $row['platNomor'] ?> - <?php echo $row['jenisTransport'] ?> </option>
                <?php
                endwhile;
                ?>
            </select>
            </div>

            <div class="col-sm-6">
            <label class="name-input">Jumlah</label>
            <input type="number" name="jumlah-alat" class="form-control">
            </div>
            
            

            </div>

            <hr class="my-4">
            
            <button type="submit" name="submit" class="w-100 btn btn-primary btn-lg" > Tambah Data </button>
            
            </form>

        
    </div>
</body>
<footer class="footer">
        <div class="py-3 fixed-bottom">
            <ul class="nav  justify-content-center border-bottom pb-3 mb-3">
                <li class="nav-item"><a href="https://www.instagram.com/damkarjogjaistimewa/" class="nav-link px-2 text-muted">Sosial</a></li>
                <li class="nav-item"><a href="https://kebakaran.jogjakota.go.id/page/index/hubungi-kami" class="nav-link px-2 text-muted">Hubungi kami</a></li>
                <li class="nav-item"><a href="https://kebakaran.jogjakota.go.id/article" class="nav-link px-2 text-muted">Berita</a></li>
                <li class="nav-item"><a href="https://kebakaran.jogjakota.go.id/page/index/daftar-layanan" class="nav-link px-2 text-muted">Tentang</a></li>
            </ul>
            <p class="text-center text-muted">DINAS PEMADAM KEBAKARAN DAN PENYELAMATAN KOTA YOGYAKARTA<br>© 2022 Pemerintah Kota Yogyakarta</p>
        </div>
    </footer>


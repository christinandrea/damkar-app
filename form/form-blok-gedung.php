
<?php
require "../conn.php";

if(isset($_GET['idgedung'])){
    $idgedung = $_GET['idgedung'];
    $query_id = "SELECT * from dk_gedung where idGedung = '$idgedung'";
    $q = mysqli_query($conn, $query_id);
    $get = mysqli_fetch_array($q, MYSQLI_ASSOC);
}

if(isset($_POST['submit'])){
   
    $id = $_POST['idGedung'];
    $nama = $_POST['namaBlok'];
    $long = $_POST['longitude-gedung'];
    $lat = $_POST['latitude-gedung'];
    $tinggi = $_POST['tinggi-gedung'];
    $luas = $_POST['luas-gedung'];
    $lantai = $_POST['lantai-gedung'];
    $kapasitas = $_POST['kapasitas-gedung'];
    $Hidran = $_POST['hidran'];
    $konstruksi = $_POST['tipeKonstruksi'];
    $bangunan = $_POST['tipeBangunan'];
    $akses = $_POST['akses'];
    $panas = $_POST['level'];
    $fireExit = $_POST['fireExit'];
    $fireExtinguisher = $_POST['fireExt'];
    $smokeDetector = $_POST['smokeDetect'];
    $fireSprinkler = $_POST['sprinkler'];
    $heatDetector = $_POST['heatDetector'];

    if(isset($_FILES['file-gedung'])){
        $file = addslashes(file_get_contents($_FILES['file-gedung']['tmp_name']));
    }else{
        $file = '';
    }
    

    // $lantai = $_POST['lantai-gedung'];
    // $lantai = $_POST['lantai-gedung'];
    // $lantai = $_POST['lantai-gedung'];

    $query_insert = "INSERT INTO `dk_blok_gedung` 
                    (`namaGedung`, `latitude`, `longitude`, `tinggiGedung`, `luasGedung`, `jumlahLantai`, `kapasitasGedung`, `denah`, `idGedung`, `jumlahHidran`, `idTipeBangunan`, `idTipeKonstruksi`, `levelKlasifikasiResiko`, `idAksesKendaraan`,
                    `fireExit`,`fireExtinguisher`,`smokeDetector`,`fireSprinkler`,`heatDetector`) 
                    VALUES 
                    ('$nama','$lat','$long','$tinggi','$luas','$lantai','$kapasitas','$file', '$id', '$Hidran', '$bangunan', '$konstruksi', '$panas', '$akses','$fireExit','$fireExtinguisher','$smokeDetector','$fireSprinkler','$heatDetector')";

    // $insert = "INSERT INTO dk_kawasan(idKawasan,namaKawasan, luasKawasan, idTipeKawasan, levelKlasifikasiResiko, idAksesKendaraan,jumlahHidran) 
    //             VALUES('$id','$nama','$luas','$jenis','$risiko','$idkend','$jumhidran')";
    $query = mysqli_query($conn, $query_insert);

    if($query){
        echo "<script>alert('Data berhasil ditambahkan!')</script>";
        echo "<script>window.location.href='../list/list-blok-gedung.php?idgedung=".$id."'</script>";
    }else{
        echo "Failed";
    }
}
?>

<!DOCTYPE html>
<head>
    <title>
        Form Tipe Kawasan
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
        <link rel="stylesheet" href="styles.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,500;0,900;1,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    
</head>


<body>
<nav>
        <ul class="horizontal">
            <li><img src="../img/logo.png" width="47" height="57"  alt=""></li>
            <li><p>DINAS PEMADAM KEBAKARAN DAN <br>PENYELAMATAN KOTA YOGYAKARTA</p></li>
            <li><a href="../admin.php">Home</a></li>
            <li><a class="active" href="../admin.php">Form</a></li>
            <li><a href="../detil.php">Details</a></li>
            <li class="rightli" style="float:right"><a href="javascript:void(0)">Logout</a></li>
        </ul>
    </nav>
    <div class="col-md-7 col-lg-8">
        <form action="form-blok-gedung.php" method="post">    
            <div class="row g-3">
                <input type="text" name="idGedung" value="<?php echo $get['idGedung']  ?>" class="form-control" hidden >
                <div class="col-sm-6">
                    <label class="form-label">Nama Gedung</label>
                    <input type="text" name="namaGedung" value="" placeholder="<?php echo $get['namaGedung'] ?>" class="form-control" readonly="">
                </div>
                <div class="col-sm-6">
                    <label class="name-input">Nama Blok Gedung</label>
                    <input type="text" name="namaBlok" class="form-control"> 
                </div>
                <div class="col-sm-6">
                    <label class="name-input">Longitude</label>
                    <input type="text" name="longitude-gedung" class="form-control"> 
                </div>
                <div class="col-sm-6">
                    <label class="name-input">Latitude</label>
                    <input type="text" name="latitude-gedung" class="form-control"> 
                </div>
                <div class="col-sm-6">
                    <label class="name-input">Tinggi Gedung</label>
                    <input type="text" name="tinggi-gedung" class="form-control"> 
                </div>

                <div class="col-sm-6">
                    <label class="name-input">Luas Gedung</label>
                    <input type="text" name="luas-gedung" class="form-control"> 
                </div>

                <div class="col-sm-6">
                    <label class="name-input">Jumlah Lantai Gedung</label>
                    <input type="text" name="lantai-gedung" class="form-control"> 
                </div>

                <div class="col-sm-6">
                    <label class="name-input">Kapasitas Gedung</label>
                    <input type="text" name="kapasitas-gedung" class="form-control"> 
                </div>

                <div class="col-sm-6">
                    <label class="name-input">Jumlah Hidran Aktif</label>
                    <input type="text" name="hidran" class="form-control"> 
                </div>

                <div class="col-sm-6">
                    <?php
                    $data = mysqli_query($conn, "SELECT * FROM dk_tipe_konstruksi ");
                    
                    ?>
                    <label class="name-input">Tipe Konstruksi </label>
                    <select name="tipeKonstruksi" id="tipe" class="form-control">
                        <option value="">-- Pilih Tipe Konstruksi</option>
                        <?php
                            while($row = mysqli_fetch_array($data,MYSQLI_ASSOC)):; 
                        ?>
                        <option value="<?php echo $row['idTipeKonstruksi']; ?>"><?php echo $row['tipeKonstruksi'] ?></option>
                        <?php
                        endwhile;
                        ?>
                    </select>
                </div>

                <div class="col-sm-6">
                    <label class="name-input">Tipe Bangunan</label>
                    <select name="tipeBangunan" id="tipeBangunan" class="form-control">
                        <option value="">-- Pilih Tipe Bangunan</option>
                        <?php
                            $data_bangunan = mysqli_query($conn, "SELECT * FROM dk_tipe_bangunan");

                            while($row = mysqli_fetch_array($data_bangunan,MYSQLI_ASSOC)):; 
                        ?>
                        <option value="<?= $row['idTipeBangunan'];?>"> <?= $row['tipeBangunan'];?></option>
                        <?php
                            endwhile;
                        ?>
                    </select>
                </div>

                <div class="col-sm-6">
 
                
                <label class="name-input">Akses Kendaraan </label>
                <select name="akses" id="akses" class="form-control">
            <option value="">-- Pilih Akses Kendaraan</option>
            <?php
                $data = mysqli_query($conn, "SELECT * FROM dk_akses_kendaraan");
                while($row = mysqli_fetch_array($data,MYSQLI_ASSOC)):; 
                ?>
               
                <option value="<?php echo $row['idAksesKendaraan']; ?>"><?php echo $row['lebarArea'] ?> Meter</option>
                <?php
                endwhile;
                ?>
            </select>
            </div>

                <div class="col-sm-6">
                    <label class="name-input">Denah Gedung</label>
                    <input type="file" name="file-gedung" class="form-control"> 
                </div>
                
                <div class="col-sm-6">
                    <label class="name-input">Level Klasifikasi Risiko</label>
                    <select name="level" class="form-control">
                        <option value="">--Pilih Klasifikasi--</option>
                        <option value="Bahaya">Bahaya</option>
                        <option value="Rawan">Rawan</option>
                        <option value="Aman">Aman</option>
                    </select>
                </div>
                <div class="col-sm-12">
                <label class="name-input">Fasilitas Pemadam Kebakaran</label>
                <div class="col-sm-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="fireExit" value="ada" id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">
                        Fire Exit
                    </label>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="fireExt" value="ada" id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">
                        Fire Extinguisher
                    </label>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="smokeDetect" value="ada" id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">
                        Smoke Detector
                    </label>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="sprinkler" value="ada" id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">
                        Fire Sprinkler
                    </label>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="heatDetector" value="ada" id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">
                        Heat Detector
                    </label>
                    </div>
                </div>
                </div>
            <div class="col-sm-12">

            <hr class="my-4">

            </div>
            <div class="col-sm-3">

            <button type="submit" name="submit" class="btn btn-primary btn-lg" onclick="window.location.href='../list/list-kawasan.php'" > Tambah Data </button>

            </div>

            </form>
        </div>
    </div>
    <footer class="footer" style="background-image: linear-gradient(rgba(10, 75, 120,.85), rgba(10, 75, 120,.85)), url('https://kebakaran.jogjakota.go.id/assets/public/batik.png')">
        	<div class="container ">
        		<div>
                    <div class="footer-text">
                        <span>DINAS PEMADAM KEBAKARAN DAN PENYELAMATAN KOTA YOGYAKARTA © 2022 <a href="http://jogjakota.go.id" class="text-light"> Pemerintah Kota Yogyakarta</a></span><br>
                        <span>
                            Jl. Kenari No. 56 Yogyakarta                             Telp.&nbsp;(0274) 587101                             Fax.&nbsp;(0274) 587101                             Email&nbsp;:&nbsp;kebakaran@jogjakota.go.id                        </span>
                    </div>       			
        		</div>
        	</div>
    </footer>
</body>



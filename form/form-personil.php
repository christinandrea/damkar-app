<?php
include "../conn.php";

if(isset($_POST['submit'])){
    $nik  = $_POST['nik'];
    $nama = $_POST['name-personil'];
    $umur = $_POST['umur-personil'];
    $jabatan = $_POST['jabatan-personil'];
    $pos = $_POST['pos'];

    $insert = "INSERT INTO dk_personil VALUES('$nik','$nama','$umur','$jabatan','$pos')";
    $query = mysqli_query($conn, $insert);

    if($query){
        echo "<script>alert('Data berhasil ditambahkan!')</script>";
        echo "<script>window.location.href='../list/view-personil.php'</script>";
    }else{
        echo "<script>alert('Data Gagal ditambahkan!')</script>";
    }
}
?>
<!DOCTYPE html>
<head>
    <title>Form Personil DAMKAR</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
        <link rel="stylesheet" href="styles.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,500;0,900;1,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    
</head>
<nav>
        <ul class="horizontal">
            <li><img src="../img/logo.png" width="47" height="57"  alt=""></li>
            <li><p>DINAS PEMADAM KEBAKARAN DAN <br>PENYELAMATAN KOTA YOGYAKARTA</p></li>
            <li><a href="../admin.php">Home</a></li>
            <li><a href="../form.php">Form</a></li>
            <li><a class="active" href="../detil.php">Details</a></li>
            <li class="rightli" style="float:right"><a href="javascript:void(0)">Logout</a></li>
        </ul>
    </nav>
<body>
<div class="col-md-7 col-lg-8">
        <div class="row justify-content-md-center">
            <form action="form-personil.php" method="post">
            <div class="col-sm-12">
                <label class="name-input">NIK</label>
                <input type="text" name="nik" class="form-control" >
            </div>

            <div class="col-sm-12">
            <label class="name-input">Nama Personil</label>
            <input type="text" name="name-personil" class="form-control">
            </div>
           
            
            <div class="col-sm-12">
            <label class="name-input">Umur (tahun)</label>
            <input type="number" name="umur-personil" class="form-control">
            </div>
            
            <div class="col-sm-12">
            <label class="name-input">Jabatan</label>
            <select name="jabatan-personil" class="form-control">
                <option value="">--Pilih Jabatan</option>
                <option value="Komandan Regu">Komandan Regu</option>
                <option value="Wakil Komandan Regu">Wakil Komandan Regu</option>
                <option value="Koordinator Operator Penyelamatan">Koordinator Operator Penyelamatan</option>
                <option value="Koordinator Pengelolaan Kendaraan dan BBM">Koordinator Pengelolaan Kendaraan dan BBM</option>
                <option value="Koordinator Tim Humas">Koordinator Tim Humas</option>
                <option value="Pengelola Kendaraan">Pengelola Kendaraan</option>
                <option value="Pengelola BBM">Pengelola BBM</option>
                <option value="Pengelola Sarpras">Pengelola Sarpras</option>
                <option value="Driver">Driver</option>
                <option value="Anggota Regu">Anggota Regu</option>
                <option value="Anggota Humas">Anggota Humas</option>
            </select>
            </div>
           
            <div class="col-sm-12">
           
            <label class="name-input">Pos Tugas</label>
            <select name="pos" id="pos" class="form-control">
                <option value="">-- Pilih Pos DAMKAR</option>
            <?php
           $data = mysqli_query($conn, "SELECT * FROM dk_pos_damkar");
          while($row = mysqli_fetch_array($data,MYSQLI_ASSOC)):; 
          ?>
           
                <option value="<?php echo $row['idPos']; ?>"><?php echo $row['namaPos'] ?></option>
          
            <?php
            endwhile;
            ?>
              </select>
            </div>

            <div class="col-sm-12">

            <hr class="my-4">

            </div>



            <div class="col-sm-3">

                <button type="submit" name="submit" class="w-90 btn btn-primary btn-lg" > Tambah Data </button>
            </div>
            </form>
            

        </div>
    </div>
</body>
<footer class="footer" style="background-image: linear-gradient(rgba(10, 75, 120,.85), rgba(10, 75, 120,.85)), url('https://kebakaran.jogjakota.go.id/assets/public/batik.png'); position:absolute; bottom: 0;">
        	<div class="container ">
        		<div>
                    <div class="footer-text">
                        <span>DINAS PEMADAM KEBAKARAN DAN PENYELAMATAN KOTA YOGYAKARTA © 2022 <a href="http://jogjakota.go.id" class="text-light"> Pemerintah Kota Yogyakarta</a></span><br>
                        <span>
                            Jl. Kenari No. 56 Yogyakarta                             Telp.&nbsp;(0274) 587101                             Fax.&nbsp;(0274) 587101                             Email&nbsp;:&nbsp;kebakaran@jogjakota.go.id                        </span>
                    </div>       			
        		</div>
        	</div>
    </footer>
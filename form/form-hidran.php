<?php
include "../conn.php";

if(isset($_POST['submit'])){
    $lokasi = $_POST['lokasi-hidran'];
    $lat = $_POST['lat-hidran'];
    $long = $_POST['long-hidran'];
    $kategori = $_POST['kategori'];
    $komponen = $_POST['komponen'];

    $insert = "INSERT INTO dk_hidran(lokasi,latitude,longitude,kategori,idKomponen) VALUES('$lokasi','$lat','$long','$kategori','$komponen')";
    $query = mysqli_query($conn, $insert);

    if($query){
        echo "<script>alert('Data berhasil ditambahkan!')</script>";
        echo "<script>window.location.href='form-hidran.php'</script>";
    }else{
        echo "Failed";
    }
    
    
}
?>
<!DOCTYPE html>
<head>
    <title>Form Hidran</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
        <link rel="stylesheet" href="styles.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,500;0,900;1,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    
</head>
<nav>
        <ul class="horizontal">
            <li><img src="../img/logo.png" width="47" height="57"  alt=""></li>
            <li><p>DINAS PEMADAM KEBAKARAN DAN <br>PENYELAMATAN KOTA YOGYAKARTA</p></li>
            <li><a href="../admin.php">Home</a></li>
            <li><a href="../form.php">Form</a></li>
            <li><a class="active" href="../detil.php">Details</a></li>
            <li class="rightli" style="float:right"><a href="javascript:void(0)">Logout</a></li>
        </ul>
    </nav>
<body>
<div class="col-md-7 col-lg-8">
        <div class="row justify-content-md-center">
            <form action="form-hidran.php" method="post">
            <div class="col-sm-12">
                <label class="name-input">Lokasi Hidran</label>
                <input type="text" name="lokasi-hidran" class="form-control" >
            </div>

            <div class="col-sm-12">
            <label class="name-input">Latitude</label>
            <input type="text" name="lat-hidran" class="form-control">
            </div>
           
            
            <div class="col-sm-12">
            <label class="name-input">Longitude</label>
            <input type="text" name="long-hidran" class="form-control">
            </div>
            

           
            <div class="col-sm-12">
           
            <label class="name-input">Kategori</label>
            <select name="kategori" id="kategori" class="form-control">
                <option value="">-- Pilih Kategori Hidran</option>
                <option value="Hidran Aktif">Hidran Aktif</option>
                <option value="Hidran Kering">Hidran Kering</option>
    
              </select>
            </div>

            <div class="col-sm-12">
           
            <label class="name-input">Komponen</label>
            <select name="komponen" id="komponen" class="form-control">
                <option value="">-- Pilih Komponen Hidran</option>
            <?php
           $data = mysqli_query($conn, "SELECT * FROM dk_komponen_hidran");
          while($row = mysqli_fetch_array($data,MYSQLI_ASSOC)):; 
          ?>
           
                <option value="<?php echo $row['idKomponen']; ?>"><?php echo $row['jenisHidran'] ?></option>
          
            <?php
            endwhile;
            ?>
              </select>
            </div>

            <div class="col-sm-12">

            <hr class="my-4">

            </div>



            <div class="col-sm-3">

                <button type="submit" name="submit" class="w-90 btn btn-primary btn-lg" > Tambah Data </button>
            </div>
            </form>
            

        </div>
    </div>
</body>
<footer class="footer" style="background-image: linear-gradient(rgba(10, 75, 120,.85), rgba(10, 75, 120,.85)), url('https://kebakaran.jogjakota.go.id/assets/public/batik.png'); position:absolute; bottom: 0;">
        	<div class="container ">
        		<div>
                    <div class="footer-text">
                        <span>DINAS PEMADAM KEBAKARAN DAN PENYELAMATAN KOTA YOGYAKARTA © 2022 <a href="http://jogjakota.go.id" class="text-light"> Pemerintah Kota Yogyakarta</a></span><br>
                        <span>
                            Jl. Kenari No. 56 Yogyakarta                             Telp.&nbsp;(0274) 587101                             Fax.&nbsp;(0274) 587101                             Email&nbsp;:&nbsp;kebakaran@jogjakota.go.id                        </span>
                    </div>       			
        		</div>
        	</div>
    </footer>
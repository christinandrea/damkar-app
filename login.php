<?php
require_once "conn.php";

if(isset($_SESSION['username'])){
    header("Location: admin.php");
}

if(isset($_POST['submit'])){
    $uname = $_POST['username'];
    $pass = $_POST['password'];

    if($uname == "damkar" && $pass=="damkar"){
        $_SESSION['username'] = $uname;

        header("location:admin.php");
        exit();
    }else{
        echo "<script>alert('User atau Password Salah!')</script>";
        echo "<script>window.location.href='login.php'</script>";
        exit();
    }
}

?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
        <link rel="stylesheet" href="styles.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,500;0,900;1,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    
</head>
<body>
<nav>
            <ul class="horizontal">
                <li><img src="img/logo.png" width="47" height="57"  alt=""></li>
                <li><p>DINAS PEMADAM KEBAKARAN DAN <br>PENYELAMATAN KOTA YOGYAKARTA</p></li>
            </ul>
        </nav>
<div class="container">
        <form action="login.php" method="post">
            <h1 class="title" style="text-align:center; margin-top:10%">Login</h1>  
            <div class="row">
                <div class="col"></div>
                <div class="col-6">
                    <label class="name-input">Username</label>
                    <input type="text" name="username" class="form-control"> 
                </div>
                <div class="col"></div>
                </div>
                <div class="row">
                <div class="col"></div>
                <div class="col-6">
                    <label class="name-input">Password</label>
                    <input type="password" name="password" class="form-control"> 
                </div>
                <div class="col"></div>
                </div>
                <div class="row">
                <div class="col"></div>
                <div class="col-6">
                <hr class="my-4">
            
                <button type="submit" name="submit" class="w-100 btn btn-primary btn-lg" > Login</button>
                </div>
                <div class="col"></div>
                </div>

            </div>
                
            
            

            </form>
        
    </div>
    </div>
    <footer class="footer" style="background-image: linear-gradient(rgba(10, 75, 120,.85), rgba(10, 75, 120,.85)), url('https://kebakaran.jogjakota.go.id/assets/public/batik.png');position:absolute; bottom:0;">
        	<div class="container ">
        		<div>
                    <div class="footer-text">
                        <span>DINAS PEMADAM KEBAKARAN DAN PENYELAMATAN KOTA YOGYAKARTA © 2022 <a href="http://jogjakota.go.id" class="text-light"> Pemerintah Kota Yogyakarta</a></span><br>
                        <span>
                            Jl. Kenari No. 56 Yogyakarta                             Telp.&nbsp;(0274) 587101                             Fax.&nbsp;(0274) 587101                             Email&nbsp;:&nbsp;kebakaran@jogjakota.go.id                        </span>
                    </div>       			
        		</div>
        	</div>
    </footer>
</body>